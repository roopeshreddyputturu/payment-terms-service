package com.treemindtech.paymentservice.constants;

public class PaymentTermConstants {

	public static final String INVALID_REMINDER = "REMINDER DAYS MUST BE BELOW THE DAYS";
	public static final String ALREADY_EXIST = "PAYEMENT TERM EXIST.";
	public static final String INVALID_DAYS = "INVALID DAYS";
	public static final String ADDED = "PAYEMENT TERM ADDED SUCCESSFULLY";
	public static final String UPDATED = "PAYEMENT TERM UPDATED SUCCESSFULLY";
	public static final String DELETED = "PAYEMENT TERM DELETED SUCCESSFULLY";
	public static final String RECORD_NOT_EXIST = "PAYEMENT TERM NOT EXIST";
	public static final String INVALID_REMINDER_DAYS = "INVALID REMINDER DAYS";
	public static final String INVALID_CODE = "INVALID CODE";
	public static final String INTERNAL_SERVER_ERROR = "PROBLEM OCCURED PLEASE TRY AGAIN LATER";

}
