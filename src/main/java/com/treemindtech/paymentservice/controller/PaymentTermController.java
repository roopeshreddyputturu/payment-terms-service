package com.treemindtech.paymentservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.treemindtech.paymentservice.entity.PaymentTermEntity;
import com.treemindtech.paymentservice.request.AddTermRequest;
import com.treemindtech.paymentservice.response.BaseResponse;
import com.treemindtech.paymentservice.service.PaymentTermService;

@RestController
@RequestMapping(value = "/paymentTerms")
public class PaymentTermController {

	@Autowired
	private PaymentTermService paymentTermService;

	@PostMapping("/add")
	public BaseResponse<Object> addPaymentTerms(@RequestBody AddTermRequest request) {
		return paymentTermService.addPaymentTerms(request);
	}

	@PostMapping("/update")
	public BaseResponse<Object> updatePaymentTerms(@RequestBody AddTermRequest request) {
		return paymentTermService.updatePaymentTerms(request);
	}

	@GetMapping("/search/{code}")
	public BaseResponse<Object> fetchSinglePaymentTerms(@PathVariable String code) {
		return paymentTermService.fetchSinglePaymentTerms(code);
	}

	@GetMapping("/delete/{code}")
	public BaseResponse<Object> deletePaymentTerms(@PathVariable String code) {
		return paymentTermService.deletePaymentTerms(code);
	}

	@GetMapping("/getAll")
	public BaseResponse<List<PaymentTermEntity>> fetchAllPaymentTerms() {
		return paymentTermService.fetchAllPaymentTerms();
	}

}
