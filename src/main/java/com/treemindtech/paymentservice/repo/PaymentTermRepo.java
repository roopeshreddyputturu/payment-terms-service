package com.treemindtech.paymentservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.treemindtech.paymentservice.entity.PaymentTermEntity;

@Repository
public interface PaymentTermRepo extends JpaRepository<PaymentTermEntity, Integer> {

	
	public PaymentTermEntity findByCode(String code);
	
}
