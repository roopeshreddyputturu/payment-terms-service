package com.treemindtech.paymentservice.response;

import com.treemindtech.paymentservice.enums.ResponseStatus;

/**
 * 
 * common status response
 * 
 * @author roope
 *
 */
public class BaseResponse<T> {

	private T data;

	private ResponseStatus status;
	private String message;

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
