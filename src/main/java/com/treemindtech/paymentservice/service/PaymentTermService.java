package com.treemindtech.paymentservice.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.treemindtech.paymentservice.constants.PaymentTermConstants;
import com.treemindtech.paymentservice.entity.PaymentTermEntity;
import com.treemindtech.paymentservice.enums.ResponseStatus;
import com.treemindtech.paymentservice.repo.PaymentTermRepo;
import com.treemindtech.paymentservice.request.AddTermRequest;
import com.treemindtech.paymentservice.response.BaseResponse;

@Service
public class PaymentTermService {

	@Autowired
	private PaymentTermRepo paymentRepo;

	private static final Logger log = LoggerFactory.getLogger(PaymentTermService.class);

	public BaseResponse<Object> addPaymentTerms(AddTermRequest request) {
		BaseResponse<Object> re = new BaseResponse<>();
		PaymentTermEntity paymentTerm = new PaymentTermEntity();
		try {
			if (request.getCode() != null && !request.getCode().equals("") && !request.getCode().equals("null")) {
				if (request.getReminderDays() < request.getDays()) {
					paymentTerm = paymentRepo.findByCode(request.getCode());
					if (paymentTerm == null) {
						paymentTerm = new PaymentTermEntity();
						paymentTerm.setCode(request.getCode());
						paymentTerm.setDescription(request.getDescription());
						paymentTerm.setCreationDate(LocalDate.now());
						paymentTerm.setDays(request.getDays());
						paymentTerm.setReminderDays(request.getReminderDays());
						paymentRepo.saveAndFlush(paymentTerm);
						re.setStatus(ResponseStatus.Success);
						re.setMessage(PaymentTermConstants.ADDED);

					} else {
						re.setStatus(ResponseStatus.Failed);
						re.setMessage(PaymentTermConstants.ALREADY_EXIST);
						log.error(PaymentTermConstants.ALREADY_EXIST);
					}
				} else {
					re.setStatus(ResponseStatus.Failed);
					re.setMessage(PaymentTermConstants.INVALID_REMINDER);
					log.error(PaymentTermConstants.INVALID_REMINDER);
				}
			} else {
				re.setStatus(ResponseStatus.Failed);
				re.setMessage(PaymentTermConstants.INVALID_CODE);
				log.error(PaymentTermConstants.INVALID_CODE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			re.setStatus(ResponseStatus.Failed);
			re.setMessage(PaymentTermConstants.INTERNAL_SERVER_ERROR);
			log.error("failed to add payment terms :: ", e);
		}
		return re;
	}

	public BaseResponse<Object> updatePaymentTerms(AddTermRequest request) {
		BaseResponse<Object> re = new BaseResponse<>();
		PaymentTermEntity paymentTerm = new PaymentTermEntity();
		try {
			if (request.getCode() != null && !request.getCode().equals("") && !request.getCode().equals("null")) {
				paymentTerm = paymentRepo.findByCode(request.getCode());
				if (paymentTerm != null) {
					paymentTerm.setCode(request.getCode());
					if (request.getDays() > 0)
						paymentTerm.setDays(request.getDays());
					if (request.getDescription() != null && !request.getDescription().equals("")
							&& !request.getDescription().equals("null"))
						paymentTerm.setDescription(request.getDescription());
					if (request.getReminderDays() > 0 && (request.getReminderDays() < paymentTerm.getDays())) {
						paymentTerm.setReminderDays(request.getReminderDays());
						paymentRepo.saveAndFlush(paymentTerm);
						re.setStatus(ResponseStatus.Success);
						re.setMessage(PaymentTermConstants.UPDATED);
					} else {
						paymentRepo.saveAndFlush(paymentTerm);
						re.setStatus(ResponseStatus.Success);
						re.setMessage(PaymentTermConstants.UPDATED);
					}
				} else {
					re.setStatus(ResponseStatus.Failed);
					re.setMessage(PaymentTermConstants.RECORD_NOT_EXIST);
					log.error(PaymentTermConstants.RECORD_NOT_EXIST);
				}
			} else {
				re.setStatus(ResponseStatus.Failed);
				re.setMessage(PaymentTermConstants.INVALID_CODE);
				log.error(PaymentTermConstants.INVALID_CODE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			re.setStatus(ResponseStatus.Failed);
			re.setMessage(PaymentTermConstants.INTERNAL_SERVER_ERROR);
			log.error("failed to update payment terms :: ", e);
		}
		return re;
	}

	public BaseResponse<Object> deletePaymentTerms(String code) {
		BaseResponse<Object> re = new BaseResponse<>();
		PaymentTermEntity paymentTerm = new PaymentTermEntity();
		try {
			if (code != null) {
				paymentTerm = paymentRepo.findByCode(code);
				if (paymentTerm != null) {
					paymentRepo.delete(paymentTerm);
					re.setStatus(ResponseStatus.Success);
					re.setMessage(PaymentTermConstants.DELETED);
				} else {
					re.setStatus(ResponseStatus.Failed);
					re.setMessage(PaymentTermConstants.RECORD_NOT_EXIST);
					log.error(PaymentTermConstants.RECORD_NOT_EXIST);
				}
			} else {
				re.setStatus(ResponseStatus.Failed);
				re.setMessage(PaymentTermConstants.INVALID_CODE);
				log.error(PaymentTermConstants.INVALID_CODE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			re.setStatus(ResponseStatus.Failed);
			re.setMessage(PaymentTermConstants.INTERNAL_SERVER_ERROR);
			log.error("failed to update payment terms :: ", e);
		}
		return re;
	}

	public BaseResponse<Object> fetchSinglePaymentTerms(String code) {
		BaseResponse<Object> re = new BaseResponse<>();
		PaymentTermEntity paymentTerm = new PaymentTermEntity();
		try {
			if (code != null) {
				paymentTerm = paymentRepo.findByCode(code);
				if (paymentTerm != null) {
					re.setData(paymentTerm);
					re.setStatus(ResponseStatus.Success);
					re.setMessage(ResponseStatus.Success.toString());
				} else {
					re.setStatus(ResponseStatus.Failed);
					re.setMessage(PaymentTermConstants.RECORD_NOT_EXIST);
					log.error(PaymentTermConstants.RECORD_NOT_EXIST);
				}
			} else {
				re.setStatus(ResponseStatus.Failed);
				re.setMessage(PaymentTermConstants.INVALID_CODE);
				log.error(PaymentTermConstants.INVALID_CODE);
			}

		} catch (Exception e) {
			e.printStackTrace();
			re.setStatus(ResponseStatus.Failed);
			re.setMessage(PaymentTermConstants.INTERNAL_SERVER_ERROR);
			log.error("failed to fetch payment terms :: ", e);
		}
		return re;
	}

	public BaseResponse<List<PaymentTermEntity>> fetchAllPaymentTerms() {
		BaseResponse<List<PaymentTermEntity>> re = new BaseResponse<>();
		List<PaymentTermEntity> paymentTerm = new ArrayList<>();
		try {
			paymentTerm = paymentRepo.findAll();
			if (!paymentTerm.isEmpty()) {
				re.setData(paymentTerm);
				re.setStatus(ResponseStatus.Success);
				re.setMessage(ResponseStatus.Success.toString());
			} else {
				re.setStatus(ResponseStatus.Failed);
				re.setMessage(PaymentTermConstants.RECORD_NOT_EXIST);
				log.error(PaymentTermConstants.RECORD_NOT_EXIST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			re.setStatus(ResponseStatus.Failed);
			re.setMessage(PaymentTermConstants.INTERNAL_SERVER_ERROR);
			log.error("failed to fetch payment terms :: ", e);
		}
		return re;
	}

}
